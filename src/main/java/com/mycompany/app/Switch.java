package com.mycompany.app;

public class Switch {
    public static String getString(String test){
        switch (test) {
            case "ciao":
                break;
            default: // Noncompliant: default clause should be the last one
                break;
            case "ciao ciao":
                break;
        }
        return "ciao";
    }
}
